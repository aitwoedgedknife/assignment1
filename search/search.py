# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

# Driver method for Depth first Search
def dfsDriver(visitedSet,currentState,outputStack,problem,goalStateVisited):
    if problem.isGoalState(currentState) or goalStateVisited:
        goalStateVisited = True
        return goalStateVisited
    visitedSet.add(currentState)
    listOfSucc = problem.getSuccessors(currentState)
    listOfSucc = listOfSucc[::-1]
    #print(listOfSucc)
    for i in listOfSucc:
        #print(i)
        currentState = i[0]
        if problem.isGoalState(i[0]):
            outputStack.push(i[1])
            goalStateVisited = True
            return goalStateVisited
        if currentState not in visitedSet:
            outputStack.push(i[1])
            goalStateVisited = dfsDriver(visitedSet,i[0],outputStack,problem,goalStateVisited)
            if(not goalStateVisited):
                outputStack.pop()
            else:
                break
    return goalStateVisited


def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print "Start:", problem.getStartState()
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())
    """
    "The problem is the object of PositionSearchProblem"

    # visitedSet has records of all visited nodes
    visitedSet = set()

    # Using stack to pile up the path to the goal
    outputStack = util.Stack()

    # boolean to indicate if goal is visited
    goalStateVisited = False

    # Search Driver which drives the search algorithm and steps to next successive states
    dfsDriver(visitedSet,problem.getStartState(),outputStack,problem,goalStateVisited)
    listOfActions = []
    while not outputStack.isEmpty():
        listOfActions.append(outputStack.pop())

    # as we are using stack reversing to get the right order
    listOfActions = listOfActions[::-1]
    return listOfActions

def breadthFirstSearch(problem):
    """Search the shallowest nodes in the search tree first."""
    VisitedDict = {}
    OutputPath = []
    TraversalQueue = util.Queue()
    StartState = problem.getStartState()

    successors = problem.getSuccessors(problem.getStartState())
    for state in successors:
        VisitedDict[state[0]] = [StartState,state[1]]
        TraversalQueue.push(state)

    # using queue to traverse through tree in BFS fashion
    while not TraversalQueue.isEmpty():
        currentState = TraversalQueue.pop()
        # base case
        if problem.isGoalState(currentState[0]):
            finalState = currentState[0]
            break
        successors = problem.getSuccessors(currentState[0])
        for substate in successors:
            if not VisitedDict.has_key(substate[0]) and not substate[0] == problem.getStartState():
                VisitedDict[substate[0]] = [currentState[0],substate[1]]
                TraversalQueue.push(substate)

    # storing path in dictionary to send instructions to pacman
    while VisitedDict.has_key(finalState):
        OutputPath.append(VisitedDict[finalState][1])
        finalState = VisitedDict[finalState][0]

    # reversing the list as it will be upside down
    OutputPath = OutputPath[::-1]
    return OutputPath

def uniformCostSearch(problem):
    """Search the node of least total cost first."""
    # using priority queue in this case to reorder discovery based on cheapest cost
    OutputQueue = util.PriorityQueue()
    visitedSet = set()
    currentState = []
    StartState = problem.getStartState()
    OutputQueue.push(StartState,0)
    costDict = {}
    costDict[StartState] = [0,[]]
    visitedSet.add(StartState)
    OutputPath = []

    while not OutputQueue.isEmpty():
        currentState = OutputQueue.pop()
        if problem.isGoalState(currentState):
            OutputPath = costDict[currentState][1]
            break
        successors = problem.getSuccessors(currentState)
        for state in successors:
            newPath = costDict[currentState][1] + [state[1]]
            # computing cost for each node
            newCost = costDict[currentState][0] + state[2]

            # updating cost if the node is either new or if it is visited before with a larger cost
            if state[0] not in visitedSet or (state[0] in costDict.keys() and newCost < costDict[state[0]][0]):
                #if state[0] not in costDict.keys() or (state[0] in costDict.keys() and newCost < costDict[state[0]][0]):
                costDict[state[0]] = [newCost,newPath]
                visitedSet.add(state[0])
                OutputQueue.update(state[0],newCost)
    return OutputPath

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def nullHeuristic(fromState,toState):
    "Dummy heuristic"
    return 0

# A Star being same as UCS only with the difference that heuristic contributes
# to preempt cheaper nodes front with more precision than UCS
def aStarSearch(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    OutputQueue = util.PriorityQueue()
    visitedSet = set()
    currentState = []
    StartState = problem.getStartState()
    OutputQueue.push(StartState,0)
    costDict = {}
    costDict[StartState] = [heuristic(StartState,problem),[]]
    visitedSet.add(StartState)
    OutputPath = []

    while not OutputQueue.isEmpty():
        currentState = OutputQueue.pop()
        if problem.isGoalState(currentState):
            OutputPath = costDict[currentState][1]
            break
        successors = problem.getSuccessors(currentState)
        for state in successors:
            newPath = costDict[currentState][1] + [state[1]]
            # Only included heuristic function to UCS to make it an A Star
            newCost = costDict[currentState][0] + state[2] + heuristic(state[0],problem)-heuristic(currentState,problem)
            if state[0] not in visitedSet or (state[0] in costDict.keys() and newCost < costDict[state[0]][0]):
                costDict[state[0]] = [newCost,newPath]
                visitedSet.add(state[0])
                OutputQueue.update(state[0],newCost)
    return OutputPath


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
